package emilianwilczek;

public class Functions
{

    public static void outputArrayValues(int[] arrayParameter)
    {
        for (int i = 0; i < arrayParameter.length; i++)
        {
            System.out.println(arrayParameter[i]);
        }
    }

    public static void main(String[] args)
    {

        int[] integerArray1 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        int[] integerArray2 = {9, 8, 7, 6, 5};

        outputArrayValues(integerArray1);
        outputArrayValues(integerArray2);

    }
}
